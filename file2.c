/***********************************************************************
**                                                                    **
**  file2 : ファイルのコードチェック                                  **
**                                                                    **
**  著者  : 伊東 徳和                                                 **
**                                                                    **
**  履歴  :                                                           **
**    v1.0 ????/??/??                                                 **
**         初版                                                       **
**    v1.1 1993/07/23                                                 **
**         DOSの^ZもASCII文字に含める事にした                         **
**                                                                    **
***********************************************************************/
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>

#define	BUFF_SIZE 4096

#define TRUE      1
#define FALSE     0

#define ASCII     1
#define EUC       2
#define SJIS      3
#define JIS       4
#define EMPTY     5
#define UNKOWN    99

/* 漢字インのコード */
#define	ESC_DALLER_B		1	/* ESC-$-B	*/
#define	ESC_DALLER_ATMARK	2	/* ESC-$-@	*/

/* 漢字アウトのコード */
#define	ESC_LEFT_PARA_B		1	/* ESC-(-B	*/
#define	ESC_LEFT_PARA_J		2	/* ESC-(-J	*/

typedef unsigned char uchar;

int	kanji_in_type;
int	kanji_out_type;

main(argc,argv)
int  argc;
char *argv[];
{
    int  i;

    if ( argc<2 ) {
        printf("Usage : %s file ...\n", argv[0]);
        exit(1);
    }
    for( i=1; i<argc; i++) {
        (void)check(argv[i]);
    }
}

int  check(filename)
char *filename;
{
    FILE  *fid;
    uchar buff[BUFF_SIZE];
    int   kind, n;
    char  *f, work[256];
    struct stat stat_buff;

    if ( stat(filename, &stat_buff)==-1 ) {
	perror(filename);
	return(1);
    }
    f = "";
    if      ( stat_buff.st_mode & S_IFIFO )  f = "pipe or FIFO";
    else if ( stat_buff.st_mode & S_IFCHR )  f = "character special";
    else if ( stat_buff.st_mode & S_IFDIR )  f = "directory";
    else if ( stat_buff.st_mode & S_IFBLK )  f = "block special";
    else if ( stat_buff.st_mode & S_IFREG ) {
        if ( (fid = fopen( filename, "r"))==NULL ) {
	    perror(filename);
	    return(1);
        }
        n = fread(buff, sizeof(char), BUFF_SIZE, fid);
        fclose(fid);
        kind = check_detail(buff, n);
        switch(kind) {
        case ASCII:
	    f = "ascii text"; break;
        case SJIS:
	    f = "SJIS text"; break;
        case EUC:
	    f = "EUC text"; break;
        case JIS:
	    strcpy(work, "JIS text");
	    if ( kanji_in_type==ESC_DALLER_B )
		strcat(work, " (KI=ESC-$-B");
	    else
	    	strcat(work, " (KI=ESC-$-@");
	    if ( kanji_out_type==ESC_LEFT_PARA_B )
		strcat(work, ",KO=ESC-(-B)");
	    else
		strcat(work, ",KO=ESC-(-J)");
	    f = work;
	    break;
        case EMPTY:
	    f = "empty"; break;
        case UNKOWN:
	    f = "UNKOWN"; break;
        }
    }
    else if ( stat_buff.st_mode & S_IFLNK )  f = "symbolic link";
    else if ( stat_buff.st_mode & S_IFSOCK ) f = "socket";
    printf("%s:%s\t%s\n", filename, (strlen(filename)+1)<=7?"\t":"", f);
    return(0);
}

int   check_detail(buff, size)
uchar buff[];
int   size;
{
    if ( size==0 ) return(EMPTY);
    if ( is_jis_file(buff, size) ) return(JIS);
    if ( is_ascii_file(buff, size) ) return(ASCII);
    if ( is_euc_file(buff, size) ) return(EUC);
    if ( is_sjis_file(buff, size) ) return(SJIS);
    return(UNKOWN);
}

int   is_jis_file(buff, size)
uchar buff[];
int   size;
{
    int  i;

    kanji_in_type = kanji_out_type = 0;

    for (i=0; i<size; i++ ) {
	if ( (i+2) < size ) {
	    if ( is_kanji_in_code(buff[i], buff[i+1], buff[i+2]) ) {
	        i += 2;
	        continue;
	    }
	    if ( is_kanji_out_code(buff[i], buff[i+1], buff[i+2]) ) {
	        i += 2;
	        continue;
	    }
	}
        if ( is_ascii_char(buff[i]) ) continue;
	return(FALSE);
    }
    if ( kanji_in_type || kanji_out_type )
        return(TRUE);
    return(FALSE);
}

int   is_ascii_file(buff, size)
uchar buff[];
int   size;
{
    int  i;

    for(i=0; i<size; i++) {
        if ( is_ascii_char(buff[i]) ) continue;
        return(FALSE);
    }
    return(TRUE);
}

int   is_kanji_in_code(c1, c2, c3)
uchar c1, c2, c3;
{
    if      ( c1==0x1b /*ESC*/ && c2==0x24 /*$*/ && c3==0x40 /*@*/ ) {
	kanji_in_type = ESC_DALLER_B;
	return(TRUE);
    }
    else if ( c1==0x1b /*ESC*/ && c2==0x24 /*$*/ && c3==0x42 /*B*/ ) {
	kanji_in_type = ESC_DALLER_ATMARK;
	return(TRUE);
    }
    return(FALSE);
}

int   is_kanji_out_code(c1, c2, c3)
uchar c1, c2, c3;
{
    if      ( c1==0x1b /*ESC*/ && c2==0x28 /*(*/ && c3==0x42 /*B*/ ) {
	kanji_out_type = ESC_LEFT_PARA_B;
	return(TRUE);
    }
    else if ( c1==0x1b /*ESC*/ && c2==0x28 /*(*/ && c3==0x4A /*J*/ ) {
	kanji_out_type = ESC_LEFT_PARA_J;
	return(TRUE);
    }
    return(FALSE);
}

int   is_ascii_char(c)
uchar c;
{
    return( c>=0x07 && c<=0x0d || c==0x1a || c==0x1b || c>=0x20 && c<=0x7f
		? TRUE : FALSE);
}

int  is_sjis_file( buff, size)
uchar buff[];
int   size;
{
    int  i;

    if ( size<=0 ) return(FALSE);
    for ( i=0; i<size; i++) {
	if ( is_ascii_char(buff[i]) ) continue;
        if ( is_katakana_char(buff[i]) ) continue;
        if ( is_sjis_char(buff[i],buff[i+1]) ) {
	    i++;
	    continue;
	}
	return(FALSE);
    }
    return(TRUE);
}

int   is_katakana_char(c)
uchar c;
{
    if ( c>=0x0a1 && c<=0xdf ) return(TRUE);
    return(FALSE);
}

int   is_sjis_char(c1, c2)
uchar c1,c2;
{
    if ( (c1>=0x081 && c1<=0x09f) || (c1>=0xe0 && c1<=0x0ee) ) {
	if ( c2>=0x40 && c2<=0x0fc ) return(TRUE);
    }
    return(FALSE);
}

int  is_euc_file( buff, size)
uchar buff[];
int   size;
{
    int  i;

    if ( size<=0 ) return(FALSE);
    for ( i=0; i<size; i++) {
	if ( is_ascii_char(buff[i]) ) continue;
	if ( is_euc_katakana_char(buff[i], buff[i+1]) ) {
	    i++;
	    continue;
	}
        if ( is_euc_char(buff[i],buff[i+1]) ) {
	    i++;
	    continue;
	}
	return(FALSE);
    }
    return(TRUE);
}

int   is_euc_char(c1, c2)
uchar c1,c2;
{
    if ( c1>=0x0a1 && c1<=0x0fe ) {
	if ( c2>=0x0a1 && c2<=0x0fe ) return(TRUE);
    }
    return(FALSE);
}

int   is_euc_katakana_char(c1, c2)
uchar c1,c2;
{
    if ( c1==0x08e ) {
	if ( is_katakana_char(c2) ) return(TRUE);
    }
    return(FALSE);
}
